/*! jQuery.customSelect() - v0.2.4 - 2013-02-03 */

(function($){
    $.fn.extend({
        customSelect : function(options) {
            if(typeof document.body.style.maxHeight !== "undefined"){ /* filter out <= IE6 */
                var
                defaults = {
                    customClass: null,
                    mapClass:true,
                    mapStyle:true
                };
                options = $.extend(defaults, options);
                    var  customSelectWrap = $("<span class='customSelectWrap' style='display:block;position:relative'></span>");
                    $("select").wrap(customSelectWrap);
                return this.each(function() {

                    var
                    $this = $(this),
                        customSelectInnerSpan = $('<span class="customSelectInner" />'),
                        customSelectSpan = $('<span class="customSelect" />').append(customSelectInnerSpan);
                    $this.after(customSelectSpan);

                    if(options.customClass)	{ customSelectSpan.addClass(options.customClass); }
                    if(options.mapClass)	{ customSelectSpan.addClass($this.attr('class')); }
                    if(options.mapStyle)	{ customSelectSpan.attr('style', $this.attr('style')); }

                    $this.on('update',function(){
                        changed(this);
                        var selectBoxWidth = parseInt($this.outerWidth(), 10) - (parseInt(customSelectSpan.outerWidth(), 10) - parseInt(customSelectSpan.width(), 10) );
                        customSelectSpan.css({display:'block'});
                        customSelectInnerSpan.css({display:'block'});
                        var selectBoxHeight = customSelectSpan.outerHeight();
                        $this.css({'-webkit-appearance':'menulist-button','z-index':'999',left:'0',width:'100%',position:'absolute', opacity:0,height:selectBoxHeight,fontSize:customSelectSpan.css('font-size')});
                    }).on("change",changed).on('keyup',function(){
                        $this.blur(); $this.focus();
                    }).on('mousedown',function(){
                        customSelectSpan.toggleClass('customSelectOpen');
                    }).focus(function(){
                        customSelectSpan.addClass('customSelectFocus');
                    }).blur(function(){
                        customSelectSpan.removeClass('customSelectFocus customSelectOpen');
                    }).hover(function(){
                        customSelectSpan.addClass('customSelectHover');
                    },function(){
                        customSelectSpan.removeClass('customSelectHover');
                    }).trigger('update');
                });

            }
        }
    });
    function changed(e){
        var $this = $( (e.currentTarget || e) ) ;
        var currentSelected = $this.find(':selected');
        var customSelectSpan = $this.next();
        var customSelectSpanInner = customSelectSpan.children(':first');
        var html = currentSelected.html() || '&#160;';
        customSelectSpanInner.html(html).parent().addClass('customSelectChanged');
        setTimeout(function(){customSelectSpan.removeClass('customSelectOpen');},60);
    }

})(jQuery);
/*
     FILE ARCHIVED ON 04:42:45 Dec 09, 2018 AND RETRIEVED FROM THE
     INTERNET ARCHIVE ON 07:19:25 Feb 14, 2019.
     JAVASCRIPT APPENDED BY WAYBACK MACHINE, COPYRIGHT INTERNET ARCHIVE.

     ALL OTHER CONTENT MAY ALSO BE PROTECTED BY COPYRIGHT (17 U.S.C.
     SECTION 108(a)(3)).
*/
/*
playback timings (ms):
  LoadShardBlock: 596.326 (3)
  esindex: 0.003
  captures_list: 683.917
  CDXLines.iter: 9.745 (3)
  PetaboxLoader3.datanode: 1143.627 (5)
  exclusion.robots: 0.154
  exclusion.robots.policy: 0.141
  RedisCDXSource: 74.872
  PetaboxLoader3.resolve: 135.509 (4)
  load_resource: 687.187
*/
