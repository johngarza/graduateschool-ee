/*---------------------index(home) page---------------------------*/

$(function () {
    $("#home_banner").responsiveSlides({
        auto: true,
        pager: false,
        nav: true,
        speed: 2000,
        namespace: "callbacks"
    });

});
/*loading page*/
$(window).load(function(){
    setTimeout(function(){
        $('.loading_container').fadeOut(3000);
    },100);

    /*search page*/
    $(document).ready(function(){
        $('#headerSearchCTA-inner').click(function() {
            $('.headerSearchContainer').slideToggle(300).toggleClass('activeSearch');
            $('#mobile-menu .menu-trigger').stop().removeClass('open');
            $('#mobile-menu .navbar').stop().removeClass('open');
            $('body, html').stop().removeClass('scroll-hidden');
            $('#mobile-menu-inner .menu-trigger').stop().removeClass('open');
            $('#mobile-menu-inner .navbar').stop().removeClass('open');
            $('body, html').stop().removeClass('scroll-hidden-top');
        });

        /*Search Drop*/
        $('#headerSearchCTA').click(function() {
            $('.headerSearchContainer').slideToggle(300).toggleClass('activeSearch');
            $('#mobile-menu .menu-trigger').stop().removeClass('open');
            $('#mobile-menu .navbar').stop().removeClass('open');
            $('body, html').stop().removeClass('scroll-hidden');
            $('#mobile-menu-inner .menu-trigger').stop().removeClass('open');
            $('#mobile-menu-inner .navbar').stop().removeClass('open');
            $('body, html').stop().removeClass('scroll-hidden-top');
        });


        $('#search_popup').click(function(){
            /*over hide scroll for popup menu*/
            $('body').toggleClass('over_hidden');
            /*sticky popupmenu */
            $('.search_container').toggleClass('sticky');
            $('.search').toggleClass('sticky');
        });
        $('.search_container').click(function(){
            $('body').removeClass('over_hidden');
            $('.search_container').removeClass('sticky');
            $('.search').removeClass('sticky');
        });

        /* mobile-search-script */
        $('.mob_search').click(function(){
            /*over hide scroll for popup menu*/
            $('body').toggleClass('over_hidden');
            /*sticky popupmenu */
            $('.search_container').toggleClass('sticky');
            $('.search').toggleClass('sticky');
        });
        $('.search_container').click(function(){
            $('body').removeClass('over_hidden');
            $('.search_container').removeClass('sticky');
            $('.search').removeClass('sticky');
        });
    })
});


/*---------------about staff page-----------------*/

$(document).ready(function(){
    $('.block.active').children('.content').slideDown();
    $('.block a').click(function(){
        $(this).parent('.block').toggleClass('active');
        $(this).next('.content').slideToggle();
        //return false;
    });


    $('.right_block a').next().slideUp();
    $('.right_block.active a').next().slideDown();
    $('.right_block a').click(function(){
        $(this).parent('.right_block').toggleClass('active');
        $(this).parent('.right_block').find("a").show();
        $(this).next().slideToggle();
        //return false;
    });

    $('.admission_col.left').css('min-height',$('.admission_col.right').outerHeight()+70);


    /*for calendar.. same height of all div as par tr height*/
    $('.calendar table tr td .event_dates').each(function(){
        var th_ = $(this)
        th_.height(th_.parent('td').parent('tr').outerHeight())
    })
    setTimeout(function(){
        $('table').addClass( "border_class" );
    },1000)

    if($('select').length > 0){
        $('select').customSelect();
    }

    /* mobile navigation */

    $('#mobile-menu-inner .menu-trigger').click(function(){
        $(this).stop().toggleClass('open')
        $('body, html').stop().toggleClass('scroll-hidden-top');
        $('#mobile-menu-inner .navbar').stop().toggleClass('open');
        $('#mobile-menu .menu-trigger').stop().removeClass('open');
        $('#mobile-menu .navbar').stop().removeClass('open');
        $('body, html').stop().removeClass('scroll-hidden');
		$('.headerSearchContainer').slideUp(300).removeClass('activeSearch');
    })

    $('#mobile-menu .menu-trigger').click(function(){
        $(this).stop().toggleClass('open')
        $('body, html').stop().toggleClass('scroll-hidden');
        $('#mobile-menu .navbar').stop().toggleClass('open');
		$('.headerSearchContainer').slideUp(300).removeClass('activeSearch');
    })

    if($('#mobile-menu ul ul, #mobile-menu ul .mobile-megamenu').length > 0){
        $('#mobile-menu ul ul, #mobile-menu ul .mobile-megamenu ').before('<em class="submenu-caret"></em>')
    }

    $('.submenu-caret').click(function(){
        $(this).next().slideToggle();
        $(this).toggleClass('toggled');
        $(this).parent().siblings().find('ul').slideUp();
        $(this).parent().siblings().find('.mobile-megamenu').slideUp();
        $(this).parent().siblings().find('em').removeClass('toggled');
    })

    if($(window).width() <= 1024 ){
        $('.admission_col.left .degrees').addClass('graphics_b');
        $('.explore').addClass('graphics_b');
        $('.explore_student_life').addClass('graphics_b');
        $('.student_services').addClass('graphics_b');
        $('.graphics_b').each(function(){
            $(this).css('background-image','url('+$(this).find('img').attr('src')+')');
            $(this).next('img').css('opacity','0')
        });
    }


    /*----- ipad hover menu -----*/
    if ( (navigator.userAgent.match(/iPhone/i))
        || (navigator.userAgent.match(/iPod/i) )
        || (navigator.userAgent.match(/iPad/i)))
    {

        $(".nav ul.menu_container li.hover-nav > a , .click-nav .sub-nav-block > a").each(function() { // have to use an `each` here - either a jQuery `each` or a `for(...)` loop
            var onClick; // this will be a function
            var firstClick = function() {
                onClick = secondClick;
                return false;
            };
            var secondClick = function() {
                onClick = firstClick;
                return true;
            };
            onClick = firstClick;
            $(this).click(function() {
                $("nav ul li.hover-nav > a, .click-nav .sub-nav-block > a").removeClass("active");
                $(this).addClass("active");
                return onClick();
            });
        });
    }

    /*--------- blog-page-toggle-script ------------*/
    if($(window).width() <= 767 ){
        $(".topics .block:first-child h5").click(function(){
            $(".content").slideToggle("slow");
            $(".content").css('display', 'block')
        });
    }

    if($(window).width() <= 1230){
        $(".main_section.table-str-block table").wrap("<div class='overflow-scroll'> </div>");
    }
    if($(window).width() <= 1230){
        $(".main_section .map.map_animate").wrap("<div class='overflow-scroll'> </div>");
    }
    if($(window).width() <= 1230){
        $(".main_section .sruc-scroll table").unwrap("<div class='overflow-scroll'> </div>");
    }

    $(".table-str-block .sruc-scroll").has("table").addClass("tableview-block")
    $(".table-str-block .sruc-scroll-block").has("table").addClass("tableview")

    /* script for set sidebat above important dates */
   if($(window).width() <= 767 ){
       if($('.admission_col_container').length > 0){
           $('.admission_col_container').find('.admission_col.left').before($('.admission_col_container').find('.admission_col.right'));
       }
   }


});

/* table-scrolling script */
$(window).resize(function(){
    if($(window).width() <= 1230){
        setTimeout( function(){
            if($(".main_section.table-str-block table").parent().hasClass("overflow-scroll")){
                return false;
            }
            else{
                $(".main_section.table-str-block table").wrap("<div class='overflow-scroll'> </div>");
            }
        },700)
    }
    if($(window).width() <= 1230){
        setTimeout( function(){
            if($(".main_section .map.map_animate").parent().hasClass("overflow-scroll")){
                return false;
            }
            else{
                $(".main_section .map").wrap("<div class='overflow-scroll'> </div>");
            }
        },700)
    }
    if($(window).width() <= 1230){
        setTimeout( function(){
            if($(".main_section .map.map_animate").parent().hasClass("overflow-scroll")){
                return false;
            }
            else{
                $(".main_section .sruc-scroll table").unwrap("<div class='overflow-scroll'> </div>");
            }
        },700)
    }
})

/*-----------admission_entry-------------------*/

/*
     FILE ARCHIVED ON 04:42:44 Dec 09, 2018 AND RETRIEVED FROM THE
     INTERNET ARCHIVE ON 07:19:23 Feb 14, 2019.
     JAVASCRIPT APPENDED BY WAYBACK MACHINE, COPYRIGHT INTERNET ARCHIVE.

     ALL OTHER CONTENT MAY ALSO BE PROTECTED BY COPYRIGHT (17 U.S.C.
     SECTION 108(a)(3)).
*/
/*
playback timings (ms):
  LoadShardBlock: 1373.349 (3)
  esindex: 0.01
  captures_list: 1395.934
  CDXLines.iter: 15.806 (3)
  PetaboxLoader3.datanode: 1418.996 (5)
  exclusion.robots: 0.275
  exclusion.robots.policy: 0.257
  RedisCDXSource: 2.14
  PetaboxLoader3.resolve: 61.576 (2)
  load_resource: 141.874
*/
